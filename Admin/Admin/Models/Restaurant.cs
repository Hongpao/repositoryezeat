﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models
{
    public class Restaurant
    {
        public int Restaurant_id { get; set; }
        public string Restaurant_name { get; set; }
        public string Restaurant_type { get; set; }
        public string Restaurant_address { get; set; }
        public double Avg_rate { get; set; }
    }
}