﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace Admin.Models
{
    public class Booking
    {
        [Key]
        public int Book_id { get; set; }
        public int Restuarant_id { get; set; }
        public string User_id { get; set; }
        public DateTime Book_Time{ get; set; }
        public DateTime Time_Stamp { get; set; }

    }
}